getData();

async function getData(){
    // fetch('user.json')
//   .then(response => response.json())
//   .then(data => console.log(data));
    const response = await fetch('user.json');
    const data = await response.json();
    
  
  
    const customers = data.customers;
   
    // Grâce à l'API Fetch récupérez et affichez le contenu du fichier JSON dans la console (utilisateurs et animaux).
    console.log(customers);
    

    // N'affichez que la liste des utilisateurs.
    customers.forEach( customer => { console.log(customer.user_name);
        document.getElementById("username").innerHTML = document.getElementById("username").innerHTML + customer.user_name +"<br>"; 
    } );

    // let i;
    // for(i = 0;i <= customers.length;i++ ){
    //     console.log(customers[i].user_name)
    // not the right way to loop an array ?}
    
    let i, j,petArray = [];
    
    for(j in customers){
        const pets = customers[j].user_pets;
        for (i in pets) {
            petArray.push(pets[i].pet_name);
            
            }
        
    }
    
       // console.log(petArray);
// N'affichez que la liste des animaux, classés dans l'ordre alphabétique de leurs noms.
       
        petArray.sort();

        console.log(petArray);
        document.getElementById("petlist").innerHTML = petArray;

        // N'affichez dans la console que les utilisateurs qui possèdent au moins un animal.
let listCustomer =[];

customers.forEach( customer => {
    
    if (customer.user_pets.length >0){
         listCustomer.push(customer.user_name);}
        })

  console.log(listCustomer);
  document.getElementById("customerlist").innerHTML =  listCustomer;

//Ajoutez un animal Mickey, de type souris, âgé de 0.1 an à chaque utilisateur puis affichez la liste des utilisateurs.
let mickey = { pet_name: "Mickey", pet_type: "souris", pet_age: 0.1};

customers.forEach( customer => {
    customer.user_pets.push(mickey);
    console.log(customer);
    document.getElementById("userAddMickeyList").innerHTML = document.getElementById("userAddMickeyList").innerHTML + JSON.stringify(customer) + "<br><br>";
});


 
}

// En bonus, parce que le propriétaire adore les chats : affichez 5 faits au hasard concernant les chats,
// en vous appuyant sur l'API Cat Facts : https://alexwohlbruck.github.io/cat-facts/docs/

getFunFacts();

async function getFunFacts(){

    const response = await fetch('https://cat-fact.herokuapp.com/facts');
    const data = await response.json();
    console.log(data);
    data.forEach(cat => { console.log(cat.text);
        document.getElementById("cats").innerHTML = document.getElementById("cats").innerHTML + cat.text + "<br>"
    });
}